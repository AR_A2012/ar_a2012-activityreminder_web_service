package com.example.user.activityreminder;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.user.activityreminder.helper.RegisterUserClass;

import java.util.HashMap;


public class RegistrasiTU extends AppCompatActivity {

    private EditText Nama;
    //private EditText NIP;
    private EditText Email;
    private EditText Pass;
    private EditText Con_Pass;
    private Spinner spinner;
    private Spinner Status;


    String nama, email, pass, con_pass, status, type;

    private static final String URL = "http://192.168.43.195/MOBILEE/reminder/registrasi.php";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        Nama = (EditText) findViewById(R.id.reg_name);
        //NIP = (EditText) findViewById(R.id.dosen_nip);
        Email = (EditText) findViewById(R.id.reg_email);
        Pass = (EditText) findViewById(R.id.reg_password);
        Con_Pass = (EditText) findViewById(R.id.con_password);

        //untuk spinner AS
        String [] values1 =
                {"--Select user type--", "TU", "DOSEN"};
        Status = (Spinner) findViewById(R.id.status);
        ArrayAdapter<String> LTRadapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, values1);
        LTRadapter1.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);
        Status.setAdapter(LTRadapter1);
    }

    public void register(View view) {
        nama = Nama.getText().toString();
        //nip = NIP.getText().toString();
        email = Email.getText().toString();
        pass = Pass.getText().toString();
        con_pass = Con_Pass.getText().toString();
        status = Status.getSelectedItem().toString();

        if (nama.isEmpty() || email.isEmpty() || pass.isEmpty() || con_pass.isEmpty())
        {
            Toast.makeText(this, "Field Wajib masih kosong", Toast.LENGTH_LONG).show();
        }
        else if (!pass.equals(con_pass))
        {
            Toast.makeText(this, "Konfirmasi Password berbeda", Toast.LENGTH_LONG).show();
        }
        else
        {
            status = Status.getSelectedItem().toString();
            if(status.equals("TU"))
                type = "TU";
            else if (status.equals("DOSEN"))
                type = "DOSEN";
            else
            {
                Toast.makeText(this, "Silahkan pilih tipe user:", Toast.LENGTH_LONG).show();
                Intent i = new Intent(this, Login.class);
                startActivity(i);
            }


            registerUser();
            Intent i = new Intent(this, Login.class);
            startActivity(i);
        }


    }

    private void registerUser() {

        register(nama, email, pass, type);
    }

    private void register(String nama,String email, String pass, String status ) {
        class Regist extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            RegisterUserClass ruc = new RegisterUserClass();


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(RegistrasiTU.this, "Please Wait", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String, String>();
                data.put("Username", params[0]);
                //data.put("NIP", params[1]);
                data.put("Email", params[1]);
                data.put("Password", params[2]);
                data.put("Privillage", params[3]);

                String result = ruc.sendPostRequest(URL, data);

                return result;
            }
        }

        Regist ru = new Regist();
        ru.execute(nama, email, pass, status);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public
    boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}


