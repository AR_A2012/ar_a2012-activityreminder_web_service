package com.example.user.activityreminder.helper;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anae on 23/12/2015.
 */
public class menudiTU_koneksi {private static final String TAG ="menudiTU_koneksi";
    private final String URL = "http://192.168.43.195/MOBILEE/reminder/TU/menudiTU_tampil_data.php";


    //deklarasi file PHP Tabel Test
    public static final String urltampil_data= "menudiTU_tampil_data.php";

    public menudiTU_koneksi(){
        super();
    }
    /**
     * Method untuk mengirim GET reguest
     */
    public String sendGetReguest(String getUrl){
        HttpClient httpClient;
        HttpGet httpGet = new HttpGet(URL+"/"+getUrl);
        InputStream is = null;
        StringBuilder stringBuilder = new StringBuilder();

        try{
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpConnectionParams.setSoTimeout(params, 3000);
            httpClient = new DefaultHttpClient(params);
            Log.d(TAG, "Executing.........");
            HttpResponse httpResponse = httpClient.execute(httpGet);
            StatusLine statusLine = httpResponse.getStatusLine();

            if(statusLine.getStatusCode() == HttpStatus.SC_OK && httpResponse !=null){
                //mengambil response dari Server
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line = null;
                while((line = reader.readLine()) !=null){
                    stringBuilder.append(line + "\n");

                }
                is.close();
            }
        }catch (Exception e){
            Log.d(TAG,e.getMessage());
        }
        return stringBuilder.toString();

    }
    /**
     * Method untuk mengirim POST reguest ke tabel test
     */
    public int sendPostReguestTest(menudiTU_Tabel menudiTU_tabel, String url){
        int replayCode = 99;
        HttpClient httpClient;
        HttpPost post = new HttpPost(URL+"/"+url);
        List<NameValuePair> aa = new ArrayList<NameValuePair>();
        aa.add(new BasicNameValuePair("Username", menudiTU_tabel.getKode()));
        try{
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpConnectionParams.setSoTimeout(params, 3000);
            httpClient = new DefaultHttpClient(params);
            post.setEntity(new UrlEncodedFormEntity(aa));
            Log.d(TAG, "Executing Post....");
            HttpResponse httpResponse = httpClient.execute(post);
            StatusLine status = httpResponse.getStatusLine();
            if(status.getStatusCode() == HttpStatus.SC_OK){
                Log.d(TAG, "Submited Successfull......");
                replayCode = status.getStatusCode();
            }
        }catch(IOException e){
            Log.d(TAG, e.getMessage());
        }
        return replayCode;

    }
}

