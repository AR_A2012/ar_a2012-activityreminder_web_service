package com.example.user.activityreminder.tu;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.user.activityreminder.R;
import com.example.user.activityreminder.helper.CustomListAdapter;
import com.example.user.activityreminder.helper.ItemBean;

import java.util.ArrayList;

public class PilihJadwal extends AppCompatActivity implements OnItemClickListener {

    ListView lview3;
    CustomListAdapter adapter;
    private ArrayList<Object> itemList;
    private ItemBean bean;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_jadwal);

        prepareArrayLits();
        lview3 = (ListView) findViewById(R.id.listView1);
        adapter = new CustomListAdapter(this, itemList);
        lview3.setAdapter(adapter);

        lview3.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
        // TODO Auto-generated method stub
        //ItemBean bean = (ItemBean) adapter.getItem(position);
        //Toast.makeText(this, "Title => "+bean.getTitle()+" n Description => "+bean.getDescription(), Toast.LENGTH_SHORT).show();
        if (position == 0) {
            Intent i = new Intent(this, SeminarProposal.class);
            startActivity(i);
        } else if (position == 1) {
            Intent sem = new Intent(this, SeminarHasil.class);
            startActivity(sem);
        } else if (position == 2) {
            Intent sid = new Intent(this, Sidang.class);
            startActivity(sid);
        } else if (position == 3) {
            Intent rap = new Intent(this, Rapat.class);
            startActivity(rap);
        } else {
            Intent mat = new Intent(this, MengajarMatkul.class);
            startActivity(mat);
        }
    }

    /* Method used to prepare the ArrayList,
     * Same way, you can also do looping and adding object into the ArrayList.
     */
    public void prepareArrayLits() {
        itemList = new ArrayList<Object>();

        AddObjectToList(R.mipmap.sempro, "Semimar Proposal", "Teknologi Informasi");
        AddObjectToList(R.mipmap.semhas, "Seminar Hasil", "Teknologi Informasi");
        AddObjectToList(R.mipmap.sidang, "Sidang", "Teknologi Informasi");
        AddObjectToList(R.mipmap.rapat, "Rapat", "Teknologi Informasi");
        AddObjectToList(R.mipmap.matkul_1, "Mengajar Matakuliah", "Teknologi Informasi");

    }

    // Add one item into the Array List
    public void AddObjectToList(int image, String title, String desc) {
        bean = new ItemBean();
        bean.setDescription(desc);
        bean.setImage(image);
        bean.setTitle(title);
        itemList.add(bean);
    }


}







