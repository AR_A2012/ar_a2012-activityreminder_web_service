package com.example.user.activityreminder;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by User on 22/12/2015.
 */
public class splash extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splashscreen);
        // Generates a Handler to launch the About Screen
        // after 3 seconds
        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                // Starts the About Screen Activity
                startActivity(new Intent(getBaseContext(),
                        Login.class));
            }
        }, 3000L);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        // Manages auto rotation for the Splash Screen Layout
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.splashscreen);
    }
}

