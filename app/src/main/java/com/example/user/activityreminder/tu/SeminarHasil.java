package com.example.user.activityreminder.tu;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.user.activityreminder.R;
import com.example.user.activityreminder.helper.RegisterUserClass;
import com.example.user.activityreminder.helper.menudiTU_Tabel;
import com.example.user.activityreminder.helper.menudiTU_koneksi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


/**
 * Created by anae on 29/12/2015.
 */
public class SeminarHasil extends AppCompatActivity {

    private Spinner spinner;

    private DatePicker datePicker;
    private Calendar calender;
    private EditText dateView;
    private int year, month, day;

    private Button btnChangeTime;
    private TimePicker timePicker1;
    private EditText timeView;
    //private TextView time;
    private Calendar calendar;
    private String format = "";


    private int hour;
    private int min;

    static final int TIME_DIALOG_ID = 98;
    static final int DATE_DIALOG_ID = 99;

    // private EditText Pengajar;
    private EditText Tanggal;
    private EditText Waktu;

    private Spinner Sebagai;
    private Spinner Tempat;
    private EditText Nim;
    private EditText Nama;
    private EditText Judul;
    private Spinner Aktivitas;
    private EditText Audiance;

    String dosen,waktu,tanggal,nim,nama,judul,sebagai,tempat,aktivitas, audiance;

    private static final String URL = "http://192.168.43.195/MOBILEE/reminder/TU/insert_jadwal_semhas.php";


    //auto
    //masuk database
    private AutoCompleteTextView Dosen;


    //finish

    private menudiTU_koneksi koneksi;
    private menudiTU_Tabel tb_test;
    private List<menudiTU_Tabel> list;



    AutoCompleteTextView autokode;



    //finsh

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seminar_hasil);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //duty
        String [] values =
                {"Seminar Hasil"};
        spinner = (Spinner) findViewById(R.id.Aktivitas_semhas);
        ArrayAdapter<String> LTRadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, values);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);
        spinner.setAdapter(LTRadapter);


        //untuk spinner AS
        String [] values1 =
                {" ","Dosen Pembimbing 1", "Dosen Pembimbing 2", "Dosen Penguji"};
        spinner = (Spinner) findViewById(R.id.sebagai_semhas);
        ArrayAdapter<String> LTRadapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, values1);
        LTRadapter1.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);
        spinner.setAdapter(LTRadapter1);



        String [] values2 =
                {" ","TI 101", "TI 102", "TI 103", "TI 104" , "TI 105", "TI 106", "Ruang Seminar"};
        spinner = (Spinner) findViewById(R.id.tempat_semhas);
        ArrayAdapter<String> LTRadapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, values2);
        LTRadapter2.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);
        spinner.setAdapter(LTRadapter2);

        //konfigurasi date
        dateView = (EditText) findViewById(R.id.tanggal_semhas);
        calender = Calendar.getInstance();
        year = calender.get(Calendar.YEAR);
        month = calender.get(Calendar.MONTH);
        day = calender.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);


        //konfigurasi time
        timeView = (EditText) findViewById(R.id.waktu_semhas);
        calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        min = calendar.get(Calendar.MINUTE);
        showTime(hour, min);


        addListenerOnButton();
        addListerner();

        // Pengajar = (EditText) findViewById(R.id.Lecturer);
        Tanggal = (EditText) findViewById(R.id.tanggal_semhas);
        Waktu = (EditText) findViewById(R.id.waktu_semhas);
        Sebagai = (Spinner) findViewById(R.id.sebagai_semhas);
        Tempat = (Spinner) findViewById(R.id.tempat_semhas);


        Nim = (EditText) findViewById(R.id.nimas_semhas);
        Nama = (EditText) findViewById(R.id.namas_semhas);
        Judul = (EditText) findViewById(R.id.submas_semhas);
        Aktivitas = (Spinner) findViewById(R.id.Aktivitas_semhas);
        Audiance = (EditText) findViewById(R.id.Audiance_jar);


        //auto



        koneksi = new menudiTU_koneksi();

        //autokode = (AutoCompleteTextView)findViewById(R.id.listdosen);
        Dosen = (AutoCompleteTextView)findViewById(R.id.listdosen);
        Dosen.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Dosen.setText(list.get(+position).getKode());

            }
        });
        //Deklarasi Untuk menampilkan data
        new DataTestAsyncTas().execute("Data Test");
        //finish




    }



    //auto


    /**
     * Method untuk menampilkan data yang ada di database msyql
     * @param //menu
     * @return
     */


    private List<menudiTU_Tabel> prosesresponse(String response){
        List<menudiTU_Tabel>list = new ArrayList<menudiTU_Tabel>();

        try{
            JSONObject object = new JSONObject(response);
            JSONArray array = object.getJSONArray("Username");
            Log.d("", "Data Lenght : " + array.length());
            menudiTU_Tabel tb_test = null;
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                tb_test = new menudiTU_Tabel();
                tb_test.setKode(obj.getString("Username"));
                list.add(tb_test);
            }
        }catch(JSONException e){
            Log.d("", e.getMessage());
        }

        return list;

    }
    /**
     * AsyncTask Untuk menampilkan data test ke TextautoComplit
     */


    private class DataTestAsyncTas extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String response = koneksi.sendGetReguest(menudiTU_koneksi.urltampil_data);
            list = prosesresponse(response);
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            //Deklarasi Variabel tabel Test
            List<String> datatest = new ArrayList<String>();
            for (int i = 0; i < list.size(); i++) {
                datatest.add(list.get(i).getKode());

            }
            //Untuk membuat  TextAutoComplit
            Dosen.setAdapter(new ArrayAdapter<String>(SeminarHasil.this,android.R.layout.simple_list_item_1,datatest));
            super.onPostExecute(result);
        }

    }




    //finish auto


























    @SuppressWarnings("deprecation")
    private void addListerner() {
        Button btnChangeDate = (Button) findViewById(R.id.pickDate);
        btnChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }
        });
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(year).append("/")
                .append(month).append("/").append(day));
    }




    @SuppressWarnings("deprecation")
    public void addListenerOnButton() {

        btnChangeTime = (Button) findViewById(R.id.pickTime);

        btnChangeTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(TIME_DIALOG_ID);

            }

        });

    }


    @SuppressWarnings("deprecation")
    @Override
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, myDateListener, year, month, day);
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this, timePickerListener, hour, min,false);
        }
        return null;


    }





    private TimePickerDialog.OnTimeSetListener timePickerListener =   new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
            hour = selectedHour;
            min = selectedMinute;

            // set current time into textview
            //timeView.setText(new StringBuilder().append(hour).append(":").append(min));

            // set current time into timepicker
            //timePicker1.setCurrentHour(hour);
            //timePicker1.setCurrentMinute(min);*/
            showTime(hour, min);

        }
    };

    public void showTime(int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        }
        else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        timeView.setText(new StringBuilder().append(hour).append(" : ").append(min)
                .append(" ").append(format));
    }



    //ini fungsi untuk masuk kedatabase
    public void semhas(View view)
    {

        dosen = Dosen.getText().toString();
        tanggal = Tanggal.getText().toString();
        waktu = Waktu.getText().toString();
        sebagai = Sebagai.getSelectedItem().toString();
        tempat = Tempat.getSelectedItem().toString();
        nim = Nim.getText().toString();
        nama = Nama.getText().toString();
        judul = Judul.getText().toString();
        aktivitas = Aktivitas.getSelectedItem().toString();
        audiance = Audiance.getText().toString();

        if (dosen.isEmpty() || tanggal.isEmpty() || waktu.isEmpty() || sebagai.isEmpty() || tempat.isEmpty() || nim.isEmpty() || nama.isEmpty() || judul.isEmpty() ||aktivitas.isEmpty() || audiance.isEmpty()  )
        {
            Toast.makeText(this, "Field Wajib masih kosong", Toast.LENGTH_LONG).show();
        } else {

            proposaluser();
            Intent h = new Intent(this, TabelDataSemhas.class);
            startActivity(h);


        }

    }

    private void proposaluser(){

        proposal(dosen,tanggal,waktu,sebagai,tempat,nim,nama,judul,aktivitas, audiance);
    }

    private void proposal(String dosen,String tanggal,String waktu,String sebagai,String tempat, String nim,String nama,String judul, String aktivitas, String audiance) {
        class Regist extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            RegisterUserClass ruc = new RegisterUserClass();


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(SeminarHasil.this, "Please Wait", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String, String>();
                data.put("Pengajar", params[0]);
                data.put("Tanggal", params[1]);
                data.put("Waktu", params[2]);
                data.put("Sebagai", params[3]);
                data.put("Tempat", params[4]);
                data.put("Nim", params[5]);
                data.put("Nama", params[6]);
                data.put("Judul", params[7]);
                data.put("Aktivitas", params[8]);
                data.put("Audiance", params[9]);

                String result = ruc.sendPostRequest(URL, data);

                return result;
            }
        }

        Regist ru = new Regist();
        ru.execute(dosen,tanggal, waktu, sebagai, tempat, nim, nama, judul, aktivitas, audiance);
    }


    //finish








}
