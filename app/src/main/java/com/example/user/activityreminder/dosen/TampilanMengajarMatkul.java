package com.example.user.activityreminder.dosen;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.activityreminder.R;
import com.example.user.activityreminder.helper.Dosen_CustomHttpClient;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by User on 14/12/2015.
 */

public class TampilanMengajarMatkul extends ListActivity{


    //Array untuk menampung data yang di ambil dari MySQL

    //private String[] arrayduty;
    private String[] arrayas;
    private String[] arraytime;
    private String[] arraydate;
    private String[] arrayplace;
    private String[] arrayaudience;
    //private String[] arrayname;
    //private String[] arraysubject;

    //private String[] Gambar;

    Dosen_ListAdapter adapter;
    String Username;

    private static final int request_code = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        //Mengeksekusi kelas GetData untuk mengirim permintaan ke MySQL
        new GetData()
                .execute("http://192.168.43.195/MOBILEE/reminder/DOSEN/getdetailMengajar.php?Username="+Username);
        //scroll

        Intent i = getIntent();
        Username = i.getStringExtra("Username").toString();
    }

    //Method untuk mengeluarkan event saat list di click
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Toast.makeText(getBaseContext(), "Terpilih " + arrayas[position],
                Toast.LENGTH_LONG).show();


    }


    //Method untuk mengeluarkan event saat list di click

    /*@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Toast.makeText(getBaseContext(), "Terpilih " + arrayduty[position],
                Toast.LENGTH_LONG).show();

    }*/

    //Class GetData yang menuruni kelas AsyncTask untuk melakukan requset data dari internet
    private class GetData extends AsyncTask<String, Void, String> {

        // Instansiasi class dialog
        ProgressDialog dialog = new ProgressDialog(TampilanMengajarMatkul.this);
        String Content;
        String Error = null;
        // membuat object class JSONObject yang digunakan untuk menangkap data
        // dengan format json
        JSONObject jObject;
        // instansiasi class ArrayList
        ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();

        @Override
        protected String doInBackground(String... params) {
            try {
                Content = Dosen_CustomHttpClient.executeHttpGet(
                        "http://192.168.43.195/MOBILEE/reminder/DOSEN/getdetailMengajar.php?Username="+Username);
            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return Content;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // menampilkan dialog pada saat proses pengambilan data dari
            // internet
            this.dialog.setMessage("Loading Data..");
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            // menutup dialog saat pengambilan data selesai
            this.dialog.dismiss();
            if (Error != null) {
                Toast.makeText(getBaseContext(), "Error Connection Internet",
                        Toast.LENGTH_LONG).show();
            } else {
                try {
                    // instansiasi kelas JSONObject
                    jObject = new JSONObject(Content);
                    // mengubah json dalam bentuk array
                    JSONArray menuitemArray = jObject.getJSONArray("select");

                    // mendeskripsikan jumlah array yang bisa di tampung
                    //arrayduty = new String[menuitemArray.length()];
                    arrayas = new String[menuitemArray.length()];
                    arraytime = new String[menuitemArray.length()];
                    arraydate = new String[menuitemArray.length()];
                    arrayplace = new String[menuitemArray.length()];
                    arrayaudience = new String[menuitemArray.length()];
                    //arrayname = new String[menuitemArray.length()];
                    //arraysubject = new String[menuitemArray.length()];


                    // mengisi variable array dengan data yang di ambil dari
                    // internet yang telah dibuah menjadi Array
                    for (int i = 0; i < menuitemArray.length(); i++) {
                        // arrayduty[i] = menuitemArray.getJSONObject(i)
                        //       .getString("Aktivitas").toString();
                        arrayas[i] = menuitemArray.getJSONObject(i)
                                .getString("IdMatkul").toString();
                        arraytime[i] = menuitemArray.getJSONObject(i)
                                .getString("Waktu").toString();
                        arraydate[i] = menuitemArray.getJSONObject(i)
                                .getString("Tanggal").toString();
                        arrayplace[i] = menuitemArray.getJSONObject(i)
                                .getString("Tempat").toString();
                        arrayaudience[i] = menuitemArray.getJSONObject(i)
                                .getString("Audiance").toString();
                        //arrayname[i] = menuitemArray.getJSONObject(i)
                        //      .getString("Nama").toString();
                        //arraysubject[i] = menuitemArray.getJSONObject(i)
                        //      .getString("Judul").toString();

                        //Gambar[i] = "http://192.168.1.102/IcaksamaCrud/gambar/"
                        //      + menuitemArray.getJSONObject(i)
                        //    .getString("gambar").toString();
                    }
                    // instansiasi class Dosen_ListAdapter (Buka class Dosen_ListAdapter)
                    adapter = new Dosen_ListAdapter(getBaseContext(),
                            arrayas, arraytime, arraydate, arrayplace, arrayaudience);
                    setListAdapter(adapter);

                } catch (JSONException ex) {
                    Logger.getLogger(TampilanMengajarMatkul.class.getName()).log(
                            Level.SEVERE, null, ex);
                }
            }
        }
    }



    //untuk button detail mahasiswa


    public void hadir(View view)
    {

        Intent awal = new Intent(this, KehadiranSempro.class);
        awal.putExtra("Username", Username);
        awal.putExtra("Hadir", "1");
        startActivity(awal);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    //end button setail mahasiswa


}

