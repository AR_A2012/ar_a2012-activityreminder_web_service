package com.example.user.activityreminder.tu;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.user.activityreminder.R;
import com.example.user.activityreminder.helper.TabelDataSidangg;
import com.example.user.activityreminder.helper.Users;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by User on 8/11/2015.
 */
public class TabelDataSidang extends AppCompatActivity {

    String data = "";
    TableLayout tl;
    TableRow tr;
    TextView label;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampilandi_tu);



        tl = (TableLayout) findViewById(R.id.maintable);

        final TabelDataSidangg getdb = new TabelDataSidangg();
        new Thread(new Runnable() {
            public void run() {
                data = getdb.TabelDataSidangg();
                System.out.println(data);

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        ArrayList<Users> users = parseJSON(data);
                        addData(users);
                    }
                });

            }
        }).start();


        //untuk menampilkan nama dosen yang dipilih dari menudiTU
        /*Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        String qString = extras.getString("qString");
        String qString1 = extras.getString("qString1");
        final EditText editText = (EditText)  findViewById(R.id.Dosen);
        //final EditText editText1 = (EditText) findViewById(R.id.NIP);
        editText.setText(qString);
        //editText1.setText(qString1);*/

        //finish
    }

    /*public void insert_jadwal(View view) {

        Intent i = new Intent(this,Pilih_jadwal.class);
        startActivity(i);
    }*/

    public ArrayList<Users> parseJSON(String result) {
        ArrayList<Users> users = new ArrayList<Users>();
        try {
            JSONArray jArray = new JSONArray(result);
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject json_data = jArray.getJSONObject(i);
                Users user = new Users();
                // user.setId(json_data.getInt("IdJadwal"));
                // user.setPengajar(json_data.getString("Pengajar"));
                user.setActivity(json_data.getString("JenisKegiatan"));
                user.setTanggal(json_data.getString("Tanggal"));
                user.setWaktu(json_data.getString("Waktu"));
                user.setPlace(json_data.getString("Tempat"));
                users.add(user);
            }
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }
        return users;
    }

    void addHeader(){
        /** Create a TableRow dynamically **/
        tr = new TableRow(this);

        /** Creating a TextView to add to the row **/
        label = new TextView(this);
        label.setText("Activity");
        label.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        label.setPadding(5, 5, 5, 5);
        label.setBackgroundColor(Color.CYAN);
        LinearLayout Ll = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        //Ll.setPadding(10, 5, 5, 5);
        Ll.addView(label,params);
        tr.addView((View)Ll); // Adding textView to tablerow.

        /** Creating Qty Button
         TextView Pengajar = new TextView(this);
         Pengajar.setText("Pengajar");
         Pengajar.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
         TableRow.LayoutParams.WRAP_CONTENT));
         Pengajar.setPadding(5, 5, 5, 5);
         Pengajar.setBackgroundColor(Color.CYAN);
         Ll = new LinearLayout(this);
         params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
         TableRow.LayoutParams.WRAP_CONTENT);
         params.setMargins(0, 5, 5, 5);
         //Ll.setPadding(10, 5, 5, 5);
         Ll.addView(Pengajar,params);
         tr.addView((View)Ll); // Adding textview to tablerow.**/

        /** Creating Qty Button **/
        TextView Date = new TextView(this);
        Date.setText("Date");
        Date.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        Date.setPadding(5, 5, 5, 5);
        Date.setBackgroundColor(Color.CYAN);
        Ll = new LinearLayout(this);
        params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 5, 5, 5);
        //Ll.setPadding(10, 5, 5, 5);
        Ll.addView(Date,params);
        tr.addView((View)Ll); // Adding textview to tablerow.

        /** Creating Qty Button **/
        TextView Time = new TextView(this);
        Time.setText("Time");
        Time.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        Time.setPadding(5, 5, 5, 5);
        Time.setBackgroundColor(Color.CYAN);
        Ll = new LinearLayout(this);
        params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 5, 5, 5);
        //Ll.setPadding(10, 5, 5, 5);
        Ll.addView(Time,params);
        tr.addView((View)Ll); // Adding textview to tablerow.

        /** Creating Qty Button **/
        TextView place = new TextView(this);
        place.setText("Place");
        place.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        place.setPadding(5, 5, 5, 5);
        place.setBackgroundColor(Color.CYAN);
        Ll = new LinearLayout(this);
        params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 5, 5, 5);
        //Ll.setPadding(10, 5, 5, 5);
        Ll.addView(place,params);
        tr.addView((View)Ll); // Adding textview to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
    }

    @SuppressWarnings({ "rawtypes" })
    public void addData(ArrayList<Users> users) {

        addHeader();

        for (Iterator i = users.iterator(); i.hasNext();) {

            Users p = (Users) i.next();

            /** Create a TableRow dynamically **/
            tr = new TableRow(this);

            /** Creating a TextView to add to the row **/
            label = new TextView(this);
            label.setText(p.getActivity());
            //label.setId(p.getId());
            label.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            label.setPadding(5, 5, 5, 5);
            label.setBackgroundColor(Color.WHITE);
            LinearLayout Ll = new LinearLayout(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            params.setMargins(5, 2, 2, 2);
            //Ll.setPadding(10, 5, 5, 5);
            Ll.addView(label,params);
            tr.addView((View)Ll); // Adding textView to tablerow.

            /** Creating Qty Button
             TextView Pengajar = new TextView(this);
             Pengajar.setText(p.getPengajar());
             Pengajar.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
             TableRow.LayoutParams.WRAP_CONTENT));
             Pengajar.setPadding(5, 5, 5, 5);
             Pengajar.setBackgroundColor(Color.WHITE);
             Ll = new LinearLayout(this);
             params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
             TableRow.LayoutParams.WRAP_CONTENT);
             params.setMargins(0, 2, 2, 2);
             //Ll.setPadding(10, 5, 5, 5);
             Ll.addView(Pengajar,params);
             tr.addView((View)Ll); // Adding textview to tablerow.
             **/
            /** Creating Qty Button **/
            TextView Date = new TextView(this);
            Date.setText(p.getTanggal());
            Date.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            Date.setPadding(5, 5, 5, 5);
            Date.setBackgroundColor(Color.WHITE);
            Ll = new LinearLayout(this);
            params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 2, 2, 2);
            //Ll.setPadding(10, 5, 5, 5);
            Ll.addView(Date,params);
            tr.addView((View)Ll); // Adding textview to tablerow.

            /** Creating Qty Button **/
            TextView Time = new TextView(this);
            Time.setText(p.getWaktu());
            Time.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            Time.setPadding(5, 5, 5, 5);
            Time.setBackgroundColor(Color.WHITE);
            Ll = new LinearLayout(this);
            params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 2, 2, 2);
            //Ll.setPadding(10, 5, 5, 5);
            Ll.addView(Time,params);
            tr.addView((View)Ll); // Adding textview to tablerow.

            /** Creating Qty Button **/
            TextView place = new TextView(this);
            place.setText(p.getPlace());
            place.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            place.setPadding(5, 5, 5, 5);
            place.setBackgroundColor(Color.WHITE);
            Ll = new LinearLayout(this);
            params = new LinearLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 2, 2, 2);
            //Ll.setPadding(10, 5, 5, 5);
            Ll.addView(place,params);
            tr.addView((View)Ll); // Adding textview to tablerow.

            // Add the TableRow to the TableLayout
            tl.addView(tr, new TableLayout.LayoutParams(
                    TableRow.LayoutParams.FILL_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
        }
    }




}


