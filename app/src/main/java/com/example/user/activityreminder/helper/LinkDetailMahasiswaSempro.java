package com.example.user.activityreminder.helper;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Created by User on 14/1/2016.
 */
public class LinkDetailMahasiswaSempro {
    //String Username;
    public String linkDetailMahasiswaSempro(){
        try {

            HttpGet httppost;
            HttpClient httpclient;
            httpclient = new DefaultHttpClient();
            httppost = new HttpGet(
                    "http://192.168.43.195/MOBILEE/reminder/Dosen/getdetail.php"); // change this to your URL.....
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            final String response = httpclient.execute(httppost,
                    responseHandler);

            return response.trim();

        } catch (Exception e) {
            System.out.println("ERROR : " + e.getMessage());
            return "error";
        }
    }
}



