package com.example.user.activityreminder.dosen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.example.user.activityreminder.R;

/**
 * Created by User on 14/12/2015.
 */
public class Dosen_ListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] nilaias;
    private final String[] nilaitime;
    private final String[] nilaidate;
    private final String[] nilaiplace;
    private final String[] nilaiaudiance;
    //private final String[] valuesimage;

    public Dosen_ListAdapter(Context context, String[] nilaias, String[] nilaitime, String[] nilaidate, String[] nilaiplace, String[] nilaiaudiance) {
        super(context, R.layout.activity_tampilan_seminar_proposal, nilaias);
        this.context = context;
        this.nilaias = nilaias;
        this.nilaitime = nilaitime;
        this.nilaidate = nilaidate;
        this.nilaiplace = nilaiplace;
        this.nilaiaudiance = nilaiaudiance;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // tampilan listview di atur dalam listview_layout.xml yang berada dalam
        // res/layout
        View rowView = inflater
                .inflate(R.layout.activity_tampilan_seminar_proposal, parent, false);
        //TextView title = (TextView) rowView.findViewById(R.id.title);
        EditText as = (EditText) rowView.findViewById(R.id.AseditText);
        EditText time = (EditText) rowView.findViewById(R.id.TimeeditText);
        EditText date = (EditText) rowView.findViewById(R.id.DateeditText);
        EditText place = (EditText) rowView.findViewById(R.id.PlaceeditText);
        EditText audiance = (EditText) rowView.findViewById(R.id.Audiance_jar);



//		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        as.setText(nilaias[position]);
        time.setText(nilaitime[position]);
        date.setText(nilaidate[position]);
        place.setText(nilaiplace[position]);
        audiance.setText(nilaiaudiance[position]);

//        new DownloadImageTask((ImageView) rowView.findViewById(R.id.icon))
        //              .execute(valuesimage[position]);
        // imageView.setImageResource(valuesimage[position]);

        return rowView;
    }
}