package com.example.user.activityreminder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.activityreminder.dosen.PilihJadwalDosen;
import com.example.user.activityreminder.helper.LoginUserClass;
import com.example.user.activityreminder.tu.PilihJadwal;

import java.util.HashMap;

public class Login extends AppCompatActivity {

    private Button btnLogin;
    private Button btnLinkToRegister;
    private Button btnDosen;
    private EditText inputEmail;
    private EditText inputPassword;
    String Username, Password;
    private static final String URL_login = "http://192.168.43.195/MOBILEE/reminder/login.php";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnlogin);
        btnLinkToRegister = (Button) findViewById(R.id.linkregis);
        // btnDosen = (Button) findViewById(R.id.linkregisd);

    }



    public void cek_log(String cek)
    {


        if(cek.equals("TU"))
        {
            Intent awal = new Intent(this, PilihJadwal.class);
            startActivity(awal);

        }
        else if(cek.equals("DOSEN"))
        {
            Intent awal = new Intent(this,PilihJadwalDosen.class);
            awal.putExtra("Username", Username);
            startActivity(awal);
            //Toast.makeText(Login.this, "Anda belum memiliki jadwal" , Toast.LENGTH_LONG).show();
            /*final EditText email = (EditText) findViewById(R.id.email);
            String myString = email.getText().toString();
            awal.putExtra("qString", myString);*/


        }
        else
        {
            Toast.makeText(Login.this, "Maaf Password Dan Username Salah!!!!" , Toast.LENGTH_LONG).show();

        }
    }
    public void onLogin(View view)
    {
        login_click();
    }
    public void reg_tu(View view)
    {
        Intent awal = new Intent(this, RegistrasiTU.class);
        startActivity(awal);

    }




    private void login_click() {

        Username = inputEmail.getText().toString();
        Password = inputPassword.getText().toString();

        login_user(Username, Password);
    }

    private void login_user(String Username, String Password) {
        class LoginUser extends AsyncTask<String, Void, String> {
            ProgressDialog loading;
            LoginUserClass ruc = new LoginUserClass();


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Login.this, "Please Wait",null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                //valid=s;
                cek_log(s);
                Toast.makeText(Login.this, s , Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String,String>();
                data.put("Username",params[0]);
                data.put("Password",params[1]);

                String result = ruc.sendPostRequest(URL_login,data);

                return  result;
            }
        }

        LoginUser ru = new LoginUser();
        ru.execute(Username,Password);
    }

}