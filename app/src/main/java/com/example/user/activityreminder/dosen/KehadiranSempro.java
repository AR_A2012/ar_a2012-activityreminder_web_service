package com.example.user.activityreminder.dosen;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.user.activityreminder.helper.RegisterUserClass;

import java.util.HashMap;

/**
 * Created by anae on 02/01/2016.
 */
public class KehadiranSempro extends AppCompatActivity {

   /* private Button Hadir;
    private Button Tdk_Hadir;

   Button hadir;
    Button tdk_hadir; */

    String btn_hadir;
    String Username;
    String IdUser;
    String Kegiatan;

    private static final String URL = "http://192.168.43.157/MOBILEE/reminder/DOSEN/kehadiran_dosen.php";
    //private static final String URL = "http://192.168.1.109/MOBILEE/reminder/DOSEN/kehadiran_dosen.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_tampilan_dosen);



        Intent intent = getIntent();
        btn_hadir = intent.getStringExtra("Hadir");
        Username = intent.getStringExtra("Username").toString();

        kehadirandosen();




    }




    private void kehadirandosen(){
        hadirdosen(Username,btn_hadir, Kegiatan);

        Toast.makeText(KehadiranSempro.this, "BERHASIL!!!!" , Toast.LENGTH_LONG).show();

        Intent i = new Intent(this, PilihJadwalDosen.class);
        i.putExtra("Username", Username);
        startActivity(i);



    }

    private void hadirdosen(String Username,String Hadir, String Kegiatan) {
        class Regist extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            RegisterUserClass ruc = new RegisterUserClass();


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(KehadiranSempro.this, "Please Wait", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String, String>();

                data.put("Username", params[0]);

                data.put("Kehadiran", params[1]);

                data.put("Kegiatan", params[2]);

                String result = ruc.sendPostRequest(URL, data);

                return result;
            }
        }

        Regist ru = new Regist();
        ru.execute(Username,Hadir,Kegiatan);
    }



}



