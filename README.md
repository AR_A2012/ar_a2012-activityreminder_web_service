Ini adalah aplikasi Activity Reminder, yang bertujuan untuk mengirimkan jadwal dosen yang di inputkan oleh admin (Tata Usaha). Adapun jadwal yang akan dikirimkan ke dosen yaitu jadwal seminar proposal, seminar hasil, sidang, rapat, dan mengajar matakuliah.

1. Coder
* Mayya Noor Lubis - 121402021
* Siti Hasanah -121402105

2. Back End
* Kania Arfianti - 121402025
* Zahara Putri Sakila -121402019
* Misbah Hasugian - 121402017
* Hasna Susanti - 121402049

3. Designer
* Wudda Rohimah - 121402005
* Ainul Husna - 121402001
* Chairina Ulfa - 121402035
* Nani Sylviani Pasi - 12140211

Database dan PHP-nya ada di file **downloads**